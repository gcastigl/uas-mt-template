# Master Thesis Latex Template #

This is a LaTeX template for the UAS master thesis according to the twbook.cls specification.

### Compiling ###

pdflatex Masterarbeit_EN.tex

### Bug reporting ###

In case you find a bug or want to suggest an improvement just create a pull request or feel fee to email me.